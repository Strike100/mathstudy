﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Enums;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    public delegate void ChangeStateDoWork(object work);
    public delegate void StateHasBeenChanged();
    public class ObjectState
    {
        public object obj;
        public State state;
        public ChangeStateDoWork doWorkInState;
        public ChangeStateDoWork doWorkOutState;
    }
    public static class StateController
    {
        private static List<ObjectState> _objectStates = new List<ObjectState>();
        public static State CurentState { get; private set; }

        public static event StateHasBeenChanged OnStateChanged;

        public static void SetState(State newState)
        {
            if (CurentState == newState) return;
            State oldState = CurentState;

            foreach(ObjectState objectState in _objectStates)
            {
                if ((objectState.state & newState) > 0)
                {
                    objectState.doWorkInState?.Invoke(objectState.obj);
                }
                else
                {
                    if ((objectState.state & oldState) > 0)
                    {
                        objectState.doWorkOutState?.Invoke(objectState.obj);
                    }
                }
            }
            CurentState = newState;
            OnStateChanged?.Invoke();
        }
        public static void AddObject(object obj, State state, ChangeStateDoWork workIn, ChangeStateDoWork workOut)
        {
            _objectStates.Add(new ObjectState() 
            {
                obj = obj,
                state = state,
                doWorkInState = workIn,
                doWorkOutState = workOut
            });

            workOut?.Invoke(obj);
        }
    }
}
