﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Enums;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    public delegate void ResetGameModel();
    public class GameModel
    {
        private int _rangeTo = 21;
        private int _answer;
        private List<ExampleAction> _exampleActions = new List<ExampleAction>() { ExampleAction.plus, ExampleAction.minus };
        public int TrueAnswer { get; private set; } = 0;

        public event ResetGameModel OnReset;
        public int RangeTo
        {
            get
            {
                return _rangeTo - 1;
            }
            set 
            {
                _rangeTo = value + 1;
            }
        }
        
        public void SetExampleActions(List<ExampleAction> actions)
        {
            _exampleActions.Clear();

            foreach(ExampleAction act in actions)
            {
                _exampleActions.Add(act);
            }
        }

        public bool GetActionEnable(ExampleAction action)
        {
            foreach(ExampleAction act in _exampleActions)
            {
                if (action == act) return true;
            }
            return false;
        }
        public Example GenerateExample()
        {
            Random random = new Random();
            Example example = new Example();
            _answer = -1;

            if (_exampleActions.Count == 0) return example;

            example.action = _exampleActions[random.Next(_exampleActions.Count)];

            switch (example.action)
            {
                case ExampleAction.plus:
                    example.numberOne = random.Next(_rangeTo);
                    example.numberTwo = random.Next(_rangeTo - example.numberOne);
                    _answer = example.numberOne + example.numberTwo;
                    break;
                case ExampleAction.minus:
                    example.numberOne = random.Next(_rangeTo);
                    example.numberTwo = random.Next(example.numberOne);
                    _answer = example.numberOne - example.numberTwo;
                    break;
                case ExampleAction.multi:
                    example.numberOne = random.Next(_rangeTo / 2 + 1);
                    int diapazonTwo = _rangeTo / ((example.numberOne == 0) ? 1 : example.numberOne);
                    example.numberTwo = random.Next(diapazonTwo);
                    _answer = example.numberOne * example.numberTwo;
                    break;
                case ExampleAction.division:
                    example.numberTwo = 1 + random.Next(_rangeTo / 2);
                    int diapazon = _rangeTo / example.numberTwo;
                    _answer = 1 + random.Next(diapazon);
                    example.numberOne = example.numberTwo * _answer;
                    break;
            }
            
            List<int> answers = new List<int>(5);
            for (int i=0; i<3; i++)
            {
                int ans = random.Next(_rangeTo);
                if (ans == _answer)
                {
                    i--;
                }
                else
                {
                    if (answers.Contains(ans))
                    {
                        i--;
                    }
                    else
                    {
                        answers.Add(ans);
                    }
                }
            }
            answers.Add(-1);
            answers.Insert(random.Next(4), _answer);

            example.var1 = answers[0];
            example.var2 = answers[1];
            example.var3 = answers[2];
            example.var4 = answers[3];

            return example;
        }

        public bool CheckAnswer(int answer)
        {
            if (_answer == answer)
            {
                TrueAnswer++;
                return true;
            }
            TrueAnswer = 0;
            return false;
        }

        public void ResetTrueAnswer()
        {
            TrueAnswer = 0;
            OnReset?.Invoke();
        }

    }

    public struct Example
    {
        public int numberOne;
        public int numberTwo;
        public ExampleAction action;
        public int var1;
        public int var2;
        public int var3;
        public int var4;
    }
}
