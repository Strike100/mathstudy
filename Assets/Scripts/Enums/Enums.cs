using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Enums
{
    public enum State
    {
        Menu =      0b1,        // 1
        Game =      0b10,       // 2
        Setting =   0b100       // 4
    }

    public enum ExampleAction
    {
        plus,
        minus,
        multi,
        division
    }
}