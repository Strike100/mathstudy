using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Enums;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Models;
using TMPro;
using Assets.Scripts.ViewModel;

public class GameManager : MonoBehaviour
{
    
    private GameModel _gameModel;
    [SerializeField]
    private GameVM _gameVM;
    [SerializeField]
    private GameSettingVM _gameSettingVM;
    [SerializeField]
    private GameObject _gamePanel;
    [SerializeField]
    private GameObject _menuPanel;
    [SerializeField]
    private GameObject _settingPanel;
    //private PlayerPrefs _playerPrefs;

    // Start is called before the first frame update
    void Start()
    {
        //_playerPrefs = new PlayerPrefs();
        _gameModel = new GameModel();
        _gameVM.SetGameModel(_gameModel);
        _gameSettingVM.SetGameModel(_gameModel);

        ChangeStateDoWork workIn = ((obj) => 
        {
            ((GameObject)obj).SetActive(true);
            _gameVM.GenerateNewExample();
        });
        ChangeStateDoWork workOut = ((obj) => 
        {
            ((GameObject)obj).SetActive(false);
        });

        StateController.AddObject(_gamePanel, State.Game, workIn, workOut);
        workIn = ((obj) =>
        {
            ((GameObject)obj).SetActive(true);
            _gameModel.ResetTrueAnswer();
        });
        StateController.AddObject(_menuPanel, State.Menu, workIn, workOut);
        StateController.AddObject(_settingPanel, State.Setting, workIn, workOut);

        if (PlayerPrefs.HasKey(_gameSettingVM.PPKey))
        {
            _gameSettingVM.ApplyLoadSetting();
        }

        StateController.SetState(State.Menu);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetGameStatus(int state)
    {
        StateController.SetState((State)state);
    }

    public void OutGame()
    {
        Application.Quit();
    }
}
