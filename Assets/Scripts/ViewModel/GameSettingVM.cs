﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Enums;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Models;
using TMPro;

namespace Assets.Scripts.ViewModel
{
    public class GameSettingVM : MonoBehaviour
    {
        public string PPKey { get; } = "MathStudySetting";

        //[SerializeField]
        private int _range;
        private int _backgroundIndex = 0;

        private GameModel _gameModel;
        [SerializeField]
        private SpriteRenderer _backgroundSprite;
        [SerializeField]
        private Image _preView;

        [SerializeField]
        private TextMeshProUGUI _rangeValue;
        [SerializeField]
        private Toggle _plusAction;
        [SerializeField]
        private Toggle _minusAction;
        [SerializeField]
        private Toggle _multiAction;
        [SerializeField]
        private Toggle _divisionAction;
        [SerializeField]
        private List<Sprite> _background = new List<Sprite>();

        private List<ExampleAction> _exampleActions = new List<ExampleAction>();

        public void SetGameModel(GameModel gameModel)
        {
            _gameModel = gameModel;
        }

        private void OnEnable()
        {
            if (_gameModel == null) return;
            _exampleActions.Clear();

            _range = _gameModel.RangeTo;
            _rangeValue.text = _range.ToString();

            _plusAction.isOn = _gameModel.GetActionEnable(ExampleAction.plus);
            _minusAction.isOn = _gameModel.GetActionEnable(ExampleAction.minus);
            _multiAction.isOn = _gameModel.GetActionEnable(ExampleAction.multi);
            _divisionAction.isOn = _gameModel.GetActionEnable(ExampleAction.division);

            if (_plusAction.isOn) AddAction(ExampleAction.plus);
            if (_minusAction.isOn) AddAction(ExampleAction.minus);
            if (_multiAction.isOn) AddAction(ExampleAction.multi);
            if (_divisionAction.isOn) AddAction(ExampleAction.division);
        }
        public void ChangeRange(int value)
        {
            _range += value;
            if (_range < 10) _range = 10;
            _rangeValue.text = _range.ToString();
        }

        public void ChangeBackground(int value)
        {
            int oldIndex = _backgroundIndex;
            _backgroundIndex += value;
            if (_backgroundIndex < 0) _backgroundIndex = _background.Count - 1;
            if (_backgroundIndex > (_background.Count-1)) _backgroundIndex = 0;

            _backgroundSprite.sprite = _background[_backgroundIndex];
            _preView.sprite = _background[_backgroundIndex];
        }

        public void SetActionPlus()
        {
            if (_plusAction.isOn)
            {
                AddAction(ExampleAction.plus);
            }
            else
            {
                RemoveAction(ExampleAction.plus);
            }
        }

        public void SetActionMinus()
        {
            if (_minusAction.isOn)
            {
                AddAction(ExampleAction.minus);
            }
            else
            {
                RemoveAction(ExampleAction.minus);
            }
        }

        public void SetActionMulti()
        {
            if (_multiAction.isOn)
            {
                AddAction(ExampleAction.multi);
            }
            else
            {
                RemoveAction(ExampleAction.multi);
            }
        }

        public void SetActionDivision()
        {
            if (_divisionAction.isOn)
            {
                AddAction(ExampleAction.division);
            }
            else
            {
                RemoveAction(ExampleAction.division);
            }
        }
        public void ApplylSetting(bool setMenuState = true)
        {
            _gameModel.RangeTo = _range;
            _gameModel.SetExampleActions(_exampleActions);


            if (setMenuState)
            {
                StateController.SetState(State.Menu);
                SaveSetting();
            }
        }

        public void CancelSetting()
        {
            StateController.SetState(State.Menu);
        }

        public void ApplyLoadSetting()
        {
            _exampleActions.Clear();

            string[] settingString = PlayerPrefs.GetString(PPKey).Split("\n").Where(s => (!string.IsNullOrEmpty(s))).ToArray();

            for(int i=0; i<settingString.Length; i++)
            {
                string[] pair = settingString[i].Split(":");
                switch (pair[0]) 
                {
                    case "range":
                        try
                        {
                            _range = int.Parse(pair[1]);
                        }
                        catch
                        {
                            _range = 10;
                        }
                        break;
                    case "background":
                        try
                        {
                            _backgroundIndex = int.Parse(pair[1]);
                        }
                        catch
                        {
                            _backgroundIndex = 0;
                        }
                        break;
                    case "plus":
                        AddAction(ExampleAction.plus);
                        break;
                    case "minus":
                        AddAction(ExampleAction.minus);
                        break;
                    case "multi":
                        AddAction(ExampleAction.multi);
                        break;
                    case "division":
                        AddAction(ExampleAction.division);
                        break;
                }
            }
            ChangeBackground(0);
            ApplylSetting(false);
        }

        private void SaveSetting()
        {
            StringBuilder settingString = new StringBuilder();

            settingString.Append($"range:{_range}\n");
            settingString.Append($"background:{_backgroundIndex}\n");
            
            foreach(ExampleAction act in _exampleActions)
            {
                settingString.Append($"{act.ToString()}:true\n");
            }
            Debug.Log(settingString.ToString());

            PlayerPrefs.SetString(PPKey, settingString.ToString());
        }

        private void AddAction(ExampleAction action)
        {
            if (!_exampleActions.Contains(action))
            {
                _exampleActions.Add(action);
            }
        }

        private void RemoveAction(ExampleAction action)
        {
            if (_exampleActions.Contains(action))
            {
                _exampleActions.Remove(action);
            }
        }
    }
}
