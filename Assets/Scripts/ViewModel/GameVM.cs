﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Enums;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Models;
using TMPro;

namespace Assets.Scripts.ViewModel
{
    public class GameVM : MonoBehaviour
    {
        private GameModel _gameModel;
        private Example _curentExample;
        private int _trAnswer = 0;
        private int _rec = 0;

        [SerializeField]
        private Button _button1;
        [SerializeField]
        private Button _button2;
        [SerializeField]
        private Button _button3;
        [SerializeField]
        private Button _button4;
        [SerializeField]
        private TextMeshProUGUI _numberOne;
        [SerializeField]
        private TextMeshProUGUI _numberTwo;
        [SerializeField]
        private TextMeshProUGUI _action;
        [SerializeField]
        private TextMeshProUGUI _record;
        [SerializeField]
        private TextMeshProUGUI _trueAnswer;

        public void SetGameModel(GameModel gameModel)
        {
            _gameModel = gameModel;
            _gameModel.OnReset += ResetTrueAnswer;
        }

        private void OnDestroy()
        {
            _gameModel.OnReset -= ResetTrueAnswer;
        }

        public void CheckAnswer(TextMeshProUGUI answer)
        {
            int answ = int.Parse(answer.text);
            if (_gameModel.CheckAnswer(answ))
            {
                _trAnswer = _gameModel.TrueAnswer;
                if (_trAnswer > _rec)
                {
                    _rec = _trAnswer;
                    _record.text = _rec.ToString();
                }
                _trueAnswer.text = _trAnswer.ToString();
                GenerateNewExample();
            }
            else
            {
                _trAnswer = _gameModel.TrueAnswer;
                _trueAnswer.text = _trAnswer.ToString();
            }
        }

        public void GenerateNewExample()
        {
            _curentExample = _gameModel.GenerateExample();

            _button1.GetComponentInChildren<TextMeshProUGUI>().text = _curentExample.var1.ToString();
            _button2.GetComponentInChildren<TextMeshProUGUI>().text = _curentExample.var2.ToString();
            _button3.GetComponentInChildren<TextMeshProUGUI>().text = _curentExample.var3.ToString();
            _button4.GetComponentInChildren<TextMeshProUGUI>().text = _curentExample.var4.ToString();

            //_action.text = _curentExample.action == ExampleAction.plus ? "+" : "-";
            switch (_curentExample.action)
            {
                case ExampleAction.plus:
                    _action.text = "+";
                    break;
                case ExampleAction.minus:
                    _action.text = "-";
                    break;
                case ExampleAction.multi:
                    _action.text = "*";
                    break;
                case ExampleAction.division:
                    _action.text = "/";
                    break;
            }

            _numberOne.text = _curentExample.numberOne.ToString();
            _numberTwo.text = _curentExample.numberTwo.ToString();
        }

        public void ResetTrueAnswer()
        {
            _trAnswer = _gameModel.TrueAnswer;
            _trueAnswer.text = _trAnswer.ToString();
        }
    }
}
